import pyautogui
import time

def mouse_down(coords, sec):
    pyautogui.moveTo(coords[0], coords[1])
    pyautogui.mouseDown()
    time.sleep(sec)
    pyautogui.mouseUp()

def mouse_click(coords):
    pyautogui.moveTo(coords[0], coords[1]) 
    pyautogui.click()

def get_pos():
    input()
    return pyautogui.position()

if __name__ == "__main__":

    start = (317, 139)
    up = (596, 197)
    down = (596, 276)
    left = (50, 273)
    right = (145, 296)
    rotate = (49, 200)

    input()
    mouse_down(start, 0.1)
    mouse_down(up, 0.2)
    time.sleep(0.1)
    mouse_down(up, 2.5)
    mouse_down(left, 0.1)
    time.sleep(0.1)
    mouse_down(left, 0.1)
    mouse_down(left, 0.1)
    mouse_down(left, 0.1)
    mouse_down(left, 0.1)
    mouse_down(right, 0.1)
    mouse_down(right, 0.1)
    mouse_down(right, 0.1)
    #mouse_down(up, 2)
    #print(get_pos())

